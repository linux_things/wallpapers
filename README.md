# Wallpapers
This is a repository to keep my wallpapers consistent.

## Disclaimer
I own none of these wallpapers and they belong to their respective owners. These wallpapers are scraped from popular wallpaper sites, mainly wallhaven.
